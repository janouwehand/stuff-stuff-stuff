﻿using System;
using System.IO;
using System.Linq;

namespace ReplacePackageRefs
{
    internal class ConvertAction
    {
        private readonly string csproj;

        public ConvertAction(string csproj, ProjectDictionary projectDictionary)
        {
            this.csproj = csproj;
            ProjectDictionary = projectDictionary;
        }

        public bool Modified { get; private set; }

        public ProjectDictionary ProjectDictionary { get; }

        internal void Execute()
        {
            var backupFile = csproj + ".backup";

            if (File.Exists(backupFile))
            {
                throw new Exception($"Backup file '{backupFile}' found. First execute Restore. Then try again.");
            }

            Modified = false;

            var lines = File.ReadAllLines(csproj);

            Modify(lines);

            if (Modified)
            {
                Console.WriteLine($"Aangepast: {csproj}");

                if (!File.Exists(backupFile))
                {
                    File.Copy(csproj, backupFile);
                }

                File.WriteAllLines(csproj, lines);
            }
        }

        private void Modify(string[] lines)
        {
            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];

                line = Modify(line);

                if (lines[i] != line)
                {
                    Modified = true;
                }

                lines[i] = line;
            }
        }

        private string Modify(string line)
        {
            var newline = line;

            if (newline.Contains("<PackageReference"))
            {
                var key = newline
                    .Trim()
                    .Replace("<PackageReference Include=\"", "");

                var i = key.IndexOf('"');

                key = key.Substring(0, i);

                if (key.StartsWith("Haan."))
                {
                    var csproj = ProjectDictionary.ProjectFiles[key].FirstOrDefault();

                    newline = $"    <ProjectReference Include=\"{csproj}\" />";
                }
            }

            return newline;
        }

    }
}
