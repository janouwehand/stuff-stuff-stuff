﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ReplacePackageRefs
{
    internal class Program
    {
        private enum ProgramActionEnum { Convert = 1, Restore = 2, ListOfCsProj = 3 }

        private static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("NextGen PackageRef Replacer\r\n\r\nThis tool replaces all Haan-package references with project references.");

                Console.WriteLine("Execute this exe with argument1: directory containing common, legacyadapters and api.\r\n\r\nEnter:");

                Console.WriteLine("  [1] for converting Haan.-packages in all .csproj files to project references.");

                Console.WriteLine("  [2] for restoring back up files of .csproj files.");

                Console.WriteLine("  [3] for showing a list of .csproj files");

                var directory = FindDirectory(args);

                Console.WriteLine(Environment.NewLine);

                Console.WriteLine($"Directory: {directory}");

                Console.WriteLine(Environment.NewLine);

                if (!Directory.Exists(directory))
                {
                    Console.WriteLine($"Directory '{directory}' does not exist!");
                }
                else
                {
                    QueryAndExecuteAction(directory);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("\r\nFinished...");

            Console.ReadKey();
        }

        private static string FindDirectory(string[] args)
        {
            var directory = args?.FirstOrDefault();

            if (string.IsNullOrEmpty(directory))
            {
                var location = Assembly.GetExecutingAssembly().Location;

                directory = Path.GetDirectoryName(location);
            }

            return directory;
        }

        private static void QueryAndExecuteAction(string directory)
        {
            var key = Console.ReadKey();

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            if (!int.TryParse(key.KeyChar.ToString(), out var i))
            {
                return;
            }

            directory = directory ?? Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var dictionary = new ProjectDictionary();

            dictionary.Populate(directory);

            var action = (ProgramActionEnum)i;

            switch (action)
            {
                case ProgramActionEnum.Convert:

                    ExecuteConvert(dictionary);
                    break;

                case ProgramActionEnum.Restore:

                    ExecuteRestore(dictionary);
                    break;

                case ProgramActionEnum.ListOfCsProj:

                    ExecuteListOfCsProj(dictionary);
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private static void ExecuteConvert(ProjectDictionary dictionary)
        {
            foreach (var csproj in dictionary.ProjectFiles.SelectMany(x => x.Value))
            {
                new ConvertAction(csproj, dictionary).Execute();
            }
        }

        private static void ExecuteRestore(ProjectDictionary dictionary)
        {
            foreach (var csproj in dictionary.ProjectFiles.SelectMany(x => x.Value))
            {
                new RestoreAction(csproj).Execute();
            }
        }

        private static void ExecuteListOfCsProj(ProjectDictionary dictionary)
        {
            foreach (var csproj in dictionary.ProjectFiles.SelectMany(x => x.Value))
            {
                Console.WriteLine(csproj);
            }
        }


    }
}
