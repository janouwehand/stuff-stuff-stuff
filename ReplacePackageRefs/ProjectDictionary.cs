﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ReplacePackageRefs
{
    internal class ProjectDictionary
    {
        private Dictionary<string, List<string>> csprojects;

        public IReadOnlyDictionary<string, List<string>> ProjectFiles => csprojects;

        public void Populate(string directory)
        {
            csprojects = new Dictionary<string, List<string>>();

            foreach (var csproj in GetProjectFiles(directory, "*.csproj"))
            {
                var key = Path.GetFileNameWithoutExtension(csproj);

                var list = csprojects.ContainsKey(key)
                    ? csprojects[key]
                    : new List<string>();

                list.Add(csproj);

                csprojects[key] = list;

                if (list.Count > 1)
                {
                    Console.WriteLine($"Let op: er zijn {list.Count} .csproj-files gevonden voor package '{key}'!");
                }
            }
        }

        private IEnumerable<string> GetProjectFiles(string directory, string pattern)
            => Directory.EnumerateFiles(directory, pattern, SearchOption.AllDirectories);

    }
}
