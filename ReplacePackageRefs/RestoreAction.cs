﻿using System;
using System.IO;

namespace ReplacePackageRefs
{
    internal class RestoreAction
    {
        private readonly string csproj;

        public RestoreAction(string csproj)
        {
            this.csproj = csproj;
        }

        public void Execute()
        {
            var backupFile = csproj + ".backup";

            if (File.Exists(backupFile))
            {
                File.Delete(csproj);

                File.Move(backupFile, csproj);

                Console.WriteLine($"Teruggezet: {csproj}");
            }
        }

    }
}
